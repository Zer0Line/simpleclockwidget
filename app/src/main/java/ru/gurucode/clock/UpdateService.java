package ru.gurucode.clock;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.text.DateFormatSymbols;
import java.util.Calendar;

public class UpdateService extends Service {
    /**
     * Used by the AppWidgetProvider to notify the Service that the views
     * need to be updated and redrawn.
     */
    static final String ACTION_UPDATE = "ru.gurucode.clock.widget.action.UPDATE";

    private final static IntentFilter sIntentFilter;

    private final static String FORMAT_12_HOURS = "h:mm";
    private final static String FORMAT_24_HOURS = "kk:mm";

    private ComponentName widget;
    private AppWidgetManager manager;
    private RemoteViews views;

    private Typeface typeFace;

    private String mTimeFormat;
    private String mDateFormat;
    private Calendar mCalendar;
    private String mAM, mPM;

    private Paint p;
    private Canvas canvasText;
    private Bitmap bitmap;
    private Paint timePaint;
    private Paint datePaint;


    static {
        sIntentFilter = new IntentFilter();
        sIntentFilter.addAction(Intent.ACTION_TIME_TICK);
        sIntentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        sIntentFilter.addAction(Intent.ACTION_TIME_CHANGED);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        reInit();
        registerReceiver(mTimeChangedReceiver, sIntentFilter);
        widget = new ComponentName(this, SimpleClock.class);
        manager = AppWidgetManager.getInstance(this);
        views = new RemoteViews(getPackageName(), R.layout.simple_clock_layout);
        typeFace = Typeface.createFromAsset(this.getAssets(),
                "fonts/ubuntu.ttf");
        initPaint();
    }

    private void initPaint(){

        p = new Paint();

        bitmap = Bitmap.createBitmap(1280, 720, Bitmap.Config
                .ARGB_4444);
        canvasText = new Canvas(bitmap);

        timePaint = new Paint();
        timePaint.setAntiAlias(true);
        timePaint.setSubpixelText(true);
        timePaint.setTypeface(typeFace);
        timePaint.setColor(Color.WHITE);
        timePaint.setTextSize(450);

        datePaint = new Paint();
        datePaint.setAntiAlias(true);
        datePaint.setSubpixelText(true);
        datePaint.setTypeface(typeFace);
        datePaint.setColor(Color.WHITE);
        datePaint.setTextSize(100);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mTimeChangedReceiver);
    }

    /*Activity запускает службу методом startService()*/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_UPDATE.equals(intent.getAction())) {
            update();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    /*связывание компонентов и службы*/
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Запускается по бродкасту, если изменилось время.
     */
    private void update() {
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        final CharSequence time = DateFormat.format(mTimeFormat, mCalendar);
        final CharSequence date = DateFormat.format(mDateFormat, mCalendar);
        views.setImageViewBitmap(R.id.Time, convertToImg(time.toString(),
                date.toString()));

        manager.updateAppWidget(widget, views);

//        AlarmManager alarmManager = (AlarmManager) this.getSystemService
//                (Context.ALARM_SERVICE);
//
//        String nextAlarm = "error";
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            AlarmManager.AlarmClockInfo aci = alarmManager.getNextAlarmClock();
//            Date alarmDate = new Date(aci.getTriggerTime());
//            nextAlarm = DateFormat.format("hh:mm", alarmDate).toString();
//        } else {
//            nextAlarm = Settings.System.getString
//                    (getContentResolver(),
//                            Settings.System.NEXT_ALARM_FORMATTED);
//        }
//        views.setTextViewText(R.id.Time, time);
//views.setTextViewText(R.id.Date, date);

//        views.setTextViewText(R.id.Alarm, "Alram: " + nextAlarm);

//        if (!is24HourMode(this)) {
//            final boolean isMorning = ((int)mCalendar.get(Calendar.AM_PM) ==
//                    Calendar.AM);
//            views.setTextViewText(R.id.AM_PM, (isMorning ? mAM : mPM));
//        } else {
//            views.setTextViewText(R.id.AM_PM, "");
//        }
    }

    private void reInit() {
        final String[] ampm = new DateFormatSymbols().getAmPmStrings();
        mDateFormat = getString(R.string.date_format);
        mTimeFormat = is24HourMode(this) ? FORMAT_24_HOURS : FORMAT_12_HOURS;
        mCalendar = Calendar.getInstance();
        mAM = ampm[0].toLowerCase();
        mPM = ampm[1].toLowerCase();
    }

    private static boolean is24HourMode(final Context context) {
        return android.text.format.DateFormat.is24HourFormat(context);
    }

    /**
     * Automatically registered when the Service is created, and unregistered
     * when the Service is destroyed.
     */
    private final BroadcastReceiver mTimeChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(Intent.ACTION_TIME_CHANGED) ||
                    action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
                    /*
                     * The user went in and tweaked his time settings, reinitialize our
                     * date/time format strings and am/pm strings before we redraw.
                     */
                reInit();
            }

            update();
        }
    };

    //Создать картинку с текстом
    private Bitmap convertToImg(String time, String date) {

//        Paint paint2 = new Paint();
//        paint2.setColor(Color.RED);
//        paint2.setStyle(Paint.Style.FILL);
//        canvasText.drawPaint(paint2);

        canvasText.drawColor(0, PorterDuff.Mode.CLEAR);
        canvasText.drawBitmap(bitmap, 0,0, p);

        canvasText.drawText(time, 70, 360, timePaint);
        canvasText.drawText(date, 140, 600, datePaint);
        return bitmap;
    }
}